<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Rates;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class ExchangeratesController extends Controller
{
    private $status;

    public function today(){
       $this->saveToDb(json_decode($this->getUsd()));

        return  $this->getUsd();
    }

    public function getUsd(){
        $response = Http::get('https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5');
        return $response;
    }

    public function saveToDb($courseUsdData){
        $exchange = $courseUsdData[0];

        $rate = new Rates();
        $rate->ccy = $exchange->ccy;
        $rate->base_ccy = $exchange->base_ccy;
        $rate->buy = $exchange->buy;
        $rate->sale = $exchange->sale;
        $rate->saveOrFail();
    }
}
