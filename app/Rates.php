<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rates extends Model
{
    protected $table = 'rates';
    protected $fillable = ['ccy','base_ccy','buy','sale'];
}
